/*********************************************************
*
* Module Name: UDP Echo server
*
* File Name:    UDPEchoServer.c
*
* Summary:
*  This file contains the echo server code
*  The server loops forever, receives a message and echoe's it back to the client.
*
* Parameters:
*    port :   the port to use.  Port 7 is used by default.
*    debugFlag: contains two pieces of information.
*               -To log samples in an output file
*               -The debugLevel which controls how much info is displayed
*
*               To log samples, set the 8th bit of the debugFlag. E.g., 128.
*               The debugLevel is set by anding the debugFlag with 0x7f
*
*               By default, this is 129 which sets debugLevel to 1 and creates samples file.
*
*    0:  serious error messages only
*    1:  adds start msg and final results  and warnings
*    2:  shows iteration results
*    3:  debug information
*
*    Example:  Value 129 sets debugLevel to 1 and creates samples file
*
*    Random Drop Rate :  0-100 represents the random packet loss process to apply to the incoming packet stream
*
* Revisions:
*  $A0 : 2-20-2017 Version 1.2
*  $A1 : 9/7/2017  Version 1.3
*  $A2 : 928/2017  V 1.4 - added CBR mode
*  $A3 : 11/9/2017  V 1.6 - prep for window mode
*            Added the random drop param and capability
*
*********************************************************/
#include "UDPEcho.h"
#include "sessionManager.h"
#include "messages.h"

void CNTCCode();
void CatchAlarm(int ignored);
void exitProcessing(double curTime);

int sock = -1;                         /* Socket descriptor */
int bStop = 1;
FILE *newFile = NULL;
double startTime = 0.0;
double lastMessageTime =0.0;

//For all client sessions
unsigned int receivedCount = 0;
long long totalBytesReceived =0;

//$A3
unsigned int arrivalCount = 0;
unsigned int totalArtificialDropCount=0;


//Used to calculate per session updates
unsigned int numberRTTSamples = 0;
unsigned int numberOfSocketsUsed = 0;
unsigned int numberOutOfOrder=0;


long avgPing = 0.0; /* final mean */
long totalPing = 0.0;

double curRTT = 0.0;
double sumOfCurRTT = 0.0;
double meanCurRTT = 0.0; /* final mean */
double smoothedRTT = 0.0;

unsigned int debugLevel = 1;
unsigned int createDataFileFlag = 0;
unsigned int errorCount = 0;  /* each loop iteration, any/all errors increment */

unsigned int debugFlagMask =  0x00000080;

void myUsage(char *myName, int paramCount, char *myVersion)
{
  printf("Usage(V%s,%d): %s [port] [debugFlag] [dropRate] \n", myVersion,paramCount,myName);
}



/*************************************************************
*
* Function: Main program for  UDPEcho  server
*
*  Parameters:
*      serverPort : port to use
        reflects drop rate applied to arriving data.
*                   Param is a number [0, 100]
*      debugFlag =  specifies the level of debug info displayed
*      dropRate =   random packet drop rate applied to arriving data.
*              Param is a double 0 - 1.0
*              If not specified, it is 0
*
* outputs:  success or failure
*
* notes:
*
***************************************************************/
int main(int argc, char *argv[])
{
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int cliAddrLen;         /* Length of incoming message */

    char echoBuffer[ECHOMAX];        /* Buffer for echo string */
    unsigned short echoServPort;     /* Server port */
    int recvMsgSize;                 /* Size of received message */
    char dataFile[] = "EchoServer.dat";
    unsigned int debugFlag = 129;
    unsigned int loopCount = 0;
    unsigned int errorCount = 0;


    unsigned int RxSeqNumber = 0;
    int *RxSeqNumberPtr = (int *)echoBuffer;
//$A2
    unsigned short RxMode = 0;
    unsigned short *RxModePtr = (unsigned short *)&echoBuffer[4];
    session *mySession = NULL;

    int rc = 0;
    double curTime = 0.0;
//$A3
    double dropRate = 0.0;

    bStop = 0;
    startTime = timestamp();
    curTime = startTime;

    if (argc < 2)         /* Needs at least the port number */
    {
      myUsage(argv[0],argc, Version);
      exit(1);
    }

    echoServPort = atoi(argv[1]);  /* First arg:  local port */
    signal (SIGINT, CNTCCode);

    if (argc == 3) {        /* get debugFlag and set the Level */
      debugFlag = atoi(argv[2]);
      debugLevel = (debugFlag & 0x0000007f);
    }

    if (argc == 4) {
      dropRate= atof(argv[3]);
      //seed randomly
      (void)srand(getSeed());
    }


    if (debugFlag && debugFlagMask)
      createDataFileFlag = 1;
    else
      createDataFileFlag = 0;


    if (debugLevel > 0) {
      printf("UDPEchoServer(#args:%d): port:%d  debugLevel:%d debugFlag:%d createFlag:%d \n",
          argc,echoServPort, debugLevel, debugFlag, createDataFileFlag);
    }

    if (createDataFileFlag == 1 ) {
      newFile = fopen(dataFile, "w");
      if ((newFile ) == NULL) {
        printf("UDPEchoServer(%s): HARD ERROR failed fopen of file %s,  errno:%d \n",
             argv[0],dataFile,errno);
        exit(1);
      }
    }

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
//    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDPLITE)) < 0){
      errorCount++;
      printf("UDPEchoServer(%f): HARD ERROR : Failure on socket call errorCount:%d  errno:%d \n",
              curTime,errorCount,  errno);
      exit(1);
    }

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */

    rc = bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));
    /* Bind to the local address */
    if (rc < 0) {
      errorCount++;
      printf("UDPEchoServer(%f): HARD ERROR : bind to port %d error, errorCount:%d  errno:%d \n",
           curTime,echoServPort,errorCount,  errno);
      exit(1);
    }

    while ( bStop != 1)
    {
      loopCount++;
      curTime = timestamp();
      /* Set the size of the in-out parameter */
      cliAddrLen = sizeof(echoClntAddr);

      /* Block until receive message from a client */
      recvMsgSize = recvfrom(sock, echoBuffer, ECHOMAX, 0, (struct sockaddr *) &echoClntAddr, &cliAddrLen);
      if (recvMsgSize < 0)
      {
        errorCount++;
        printf("UDPEchoServer(%f)(%d) HARD ERROR : error recvfrom  errorCount%d  errno:%d \n",
              curTime,loopCount, errorCount,  errno);
        if (createDataFileFlag == 1 ) {
            fprintf(newFile,"UDPEchoServer(%f)(%d) HARD ERROR : error recvfrom  errorCount%d  errno:%d \n",
              curTime,loopCount, errorCount,  errno);
        }
        continue;
      }

      //Randomly drop the message that arrived
      //the rand() function returns a RN [0,RAND_MAX]
      if(dropRate) {
        if(rand() < (RAND_MAX * dropRate)) {
          totalArtificialDropCount++;
          continue;
        }
       arrivalCount++;
      }
      //Check if Session already exists
      lastMessageTime =curTime;
      mySession = findSession(echoClntAddr.sin_addr, echoClntAddr.sin_port);

      if(mySession == NULL) // If not added already, create a session
      {
        mySession = createSession(echoClntAddr.sin_addr, echoClntAddr.sin_port);
      }


      //Tracks total number received from all clients
      receivedCount++;
      totalBytesReceived += recvMsgSize;

      RxSeqNumber = (unsigned int)ntohl( (unsigned int)(*RxSeqNumberPtr) );

      if (mySession->messagesReceived == 0) {
        mySession->largestSeqNumRxed =  RxSeqNumber;
      }

      //two cases, arrival is a 'new' segment
      if (mySession->largestSeqNumRxed  < RxSeqNumber)
        mySession->largestSeqNumRxed = RxSeqNumber;

      //the arrival is an 'old' segment
      if (mySession->largestSeqNumRxed >= RxSeqNumber)
        mySession->outOfOrderArrivals++;


      mySession->lastSeqNumRxed = RxSeqNumber;

      RxMode = (unsigned short )ntohs( (unsigned short )(*RxModePtr));
      mySession->mode = RxMode;

      //Update session info
      gettimeofday(&(mySession->lastRxTime), NULL);
      mySession->messagesReceived++;
      mySession->bytesReceived += recvMsgSize;

      mySession->messagesLost = mySession->largestSeqNumRxed - mySession->messagesReceived;


      if (debugLevel > 2) {
        printf("UDPEchoServer(%f):(%d,%d),rxed %d bytes, RxSeqNumber:%d Rxmode:%d errorCount:%d #sessions:%d \n",
            curTime,loopCount,receivedCount, recvMsgSize, RxSeqNumber, RxMode,errorCount, getNumberSessions());
        displaySession(mySession,FALSE);
      }


      if (RxMode == RTT_MODE) {
        /* Send received datagram back to the client */
        rc = sendto(sock, echoBuffer, recvMsgSize, 0,  (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr));

        if ( rc != recvMsgSize)
        {
          errorCount++;
          printf("UDPEchoServer(%f):(%d) HARD ERROR : error sending %d bytes to client %s, errorCount%d, rc:%d  errno:%d \n",
              curTime,loopCount,recvMsgSize, inet_ntoa(echoClntAddr.sin_addr), errorCount, rc,  errno);
          if (createDataFileFlag == 1 ) {
            fprintf(newFile,"UDPEchoServer(%f):(%d) HARD ERROR : error sending %d bytes to client %s, errorCount%d  rc:%d, errno:%d \n",
              curTime,loopCount,recvMsgSize, inet_ntoa(echoClntAddr.sin_addr), errorCount, rc,  errno);
          }
          continue;
        }

        if (debugLevel > 1) {
          printf("UDPEchoServer(%f):(%d,%d), ECHOED  %d bytes, SeqNumber:%d sessionLargestSeqNumber/total:%d %d  RxMode:%d errorCount:%d #sessions:%d \n",
            curTime,loopCount,receivedCount,recvMsgSize, RxSeqNumber, mySession->largestSeqNumRxed, mySession->messagesReceived, RxMode, errorCount, getNumberSessions());
        }
        if (createDataFileFlag == 1 ) {
          fprintf(newFile,"%f %d %d %d %d %d %d %d\n",
               curTime,RxSeqNumber,recvMsgSize, RxMode, mySession->largestSeqNumRxed, mySession->messagesReceived,errorCount, getNumberSessions());
        }

      } else if (RxMode == CBR_MODE) {
        //Do nothing.....
        lastMessageTime =curTime;

      } else if (RxMode == WINDOW_MODE){
        // @TODO: add code
          struct DataMsg* myDataMsg;
          struct ACKMsg* myAckMsg;
          struct MsgHdr* myHdr;

          struct timeval currentTime;
          gettimeofday(&currentTime, NULL);
          unpackNetworkBufferToDataMsg(&myDataMsg, echoBuffer, recvMsgSize);
          if(myDataMsg->myHdr->sequenceNum == mySession->nextExpectedSeqNumber){
            ++mySession->nextExpectedSeqNumber;
          }
          myHdr =createMsgHdr(mySession->nextExpectedSeqNumber,RxMode,ACKMSG,currentTime.tv_sec, currentTime.tv_usec);
          myAckMsg = createACKMsg(myHdr, mySession->nextExpectedSeqNumber);
          packACKMSGToNetworkBuffer(myAckMsg, echoBuffer, ACKMsgSize);

          /* Send received datagram back to the client */
          // rc = sendto(sock, myAckMsg, sizeof(myAckMsg), 0,  (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr));
            rc = sendto(sock, echoBuffer, ACKMsgSize,  0,  (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr));

          if (debugLevel > 1) {
              printf("UDPEchoServer(%f):(%d,%d), ECHOED  %d bytes, SeqNumber:%d sessionLargestSeqNumber/total:%d %d  RxMode:%d errorCount:%d #sessions:%d \n",
                     curTime,loopCount,receivedCount,recvMsgSize, RxSeqNumber, mySession->largestSeqNumRxed, mySession->messagesReceived, RxMode, errorCount, getNumberSessions());
          }
          if (createDataFileFlag == 1 ) {
              fprintf(newFile,"%f %d %d %d %d %d %d %d\n",
                      curTime,RxSeqNumber,recvMsgSize, RxMode, mySession->largestSeqNumRxed, mySession->messagesReceived,errorCount, getNumberSessions());
          }


          // @TODO: We need to add more code for retransmition of things


        //
      }else {
        printf("UDPEchoServer:(%f): HARD ERROR: Bad mode :  %d \n",
           curTime, RxMode);
        exit(1);
      }
    }

    //If we get here, we are supposed to end....
    printf("UDPEchoServer:(%f)(%d,%d) WARNING:  Ending Loop  \n", curTime,loopCount,receivedCount);
    //exitProcessing(curTime);
    exit(0);
}


void CNTCCode() {

  bStop = 1;
  exitProcessing(timestamp());
  exit(0);
}


void exitProcessing(double curTime)
{

  double  duration;
  double totalThroughput=0.0;
  double artificialDropRate =0.0;
//double endTime = curTime;
// duration = endTime - startTime;
  duration = lastMessageTime - startTime;

  if (sock != -1)
    close(sock);
  sock = -1;

  if (duration > 0.0)
    totalThroughput = ((double )totalBytesReceived)*8 / duration;

  if (arrivalCount > 0)
     artificialDropRate =  (double)totalArtificialDropCount / (double)arrivalCount;

  if (debugLevel >0) {
    printf("UDPEcho2Server(%f secs, #sessions:%d) receivedCount:%d, totalBytesRxed:%lld, aggregateThroughput: %9.0f, artificialDropRate=:%0.3f \n",
       duration, getNumberSessions(), receivedCount, totalBytesReceived, totalThroughput,artificialDropRate);
  }

  displayAllSessions(FALSE);

  if (newFile != NULL) {
    fflush(newFile);
    fclose(newFile);
  }
}
